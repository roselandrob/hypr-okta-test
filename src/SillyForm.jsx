import React, { useState, useEffect } from 'react';
import { useOktaAuth } from '@okta/okta-react';
import './App.css';

function pollForMfaCheck(url, headers, body, retries = 50, setResult, setCurRequestId){
  const retryCodes = [401]
  return fetch(url, {
    method: 'POST', 
    body: JSON.stringify(body), 
    headers: headers
  })
    .then(res => {
      if (res.ok){
        return res.json();
      }
      if (retries > 0 && retryCodes.includes(res.status)){
        setTimeout(() => {
          return pollForMfaCheck(url, headers, body, retries-1, setResult, setCurRequestId)
        }, 15);
      }
      else {
        throw new Error(res);
      }
     })
     .then(json => {
       console.log(json.message);
       setResult(json.message);
       setCurRequestId('')
     })
     .catch(err => {
      console.error(err);
      if (retries == 0){
        setResult("MFA Retries exhausted.");
        setCurRequestId('');
      }
     })
}

function SillyForm(props){
  const { authState, authService } = useOktaAuth();
  const [curName, setName] = useState('')
  const [curCity, setCity] = useState('')
  const [curResult, setResult] = useState('')
  const [userInfo, setUserInfo] = useState(null);
  const [curRequestId, setCurRequestId] = useState('')
  
  useEffect(() => {
    if (!authState.isAuthenticated) {
      // When user isn't authenticated, forget any user info
      setUserInfo(null);
    } else {
      if (userInfo == null){
        authService.getUser().then((info) => {
          setUserInfo(info);
        });
      }
    }
    if (userInfo != null){
      if (curRequestId !=='') {
        var accessToken = authState.accessToken;
        var input = {
          name: curName, 
          city: curCity, 
          requestId: curRequestId
        }
        pollForMfaCheck('https://k8r392e5ha.execute-api.us-east-2.amazonaws.com/dev/mfa_check', { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken}, input, 50, setResult, setCurRequestId);
      }
    }
  }, [authState, authService, curRequestId]); // Update if authState changes

  if (!userInfo) {
    return (
      <div>
        <p>Fetching user info...</p>
      </div>
    );
  }

  if (curRequestId !== ''){
    return (
      <div>
        <p>Awaiting MFA....</p>
      </div>
    )
  }

  const resultRendered = [curResult].map((str) => <pre key={str}>{str}</pre>) 
  return (
    <div>
      <h1>Rob's SPA</h1>
      <form onSubmit={(event)=>handleSubmit(event, curName, curCity, setCurRequestId, userInfo, authState)}>
        <label>
          Name:
        <input type="text" value={curName} onChange={(event)=>handleNameChange(event, curName, setName)} />
        </label>
        <label>
          City:
        <input type="text" value={curCity} onChange={(event)=>handleCityChange(event, curCity, setCity)} />
        </label>
        <input type="submit" value="Submit" />
      </form>
      <br />
      {resultRendered}
    </div>
  );
}
  
function handleNameChange(event, curName, setName) {
  setName(event.target.value);
}

function handleCityChange(event, curCity, setCity) {
  setCity(event.target.value);
}

function handleSubmit(event, curName, curCity, setCurRequestId, userInfo, authState) {
  alert('A name was submitted: ' + curName + ' with city ' + curCity);
  var input = {
    name: curName, 
    city: curCity, 
  };
  alert(JSON.stringify(input));
  fetch('https://k8r392e5ha.execute-api.us-east-2.amazonaws.com/dev/mfa_push', {
    method: 'POST', 
    body: JSON.stringify(input),
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+ authState.accessToken},
  })
  .then(response => response.json())
  .then(data => data.requestId)
  .then(requestId => 
    setCurRequestId(requestId)
  )
  .catch(error=>{
    console.error(error);
  })
  event.preventDefault();
}

export default SillyForm;
  